## Design pattern

We use the Atomic Design pattern to structure our code. Please, see this [documentation](https://medium.com/@janelle.wg/atomic-design-pattern-how-to-structure-your-react-application-2bb4d9ca5f97#:~:text=The%20Atomic%20design%20pattern%20has,the%20componentised%20nature%20of%20React.&text=Atomic%20design%2C%20developed%20by%20B,consistency%2C%20modularity%2C%20and%20scalability. 'Atomic Design Pattern explanations') for more informations.

## Folders

- atoms:

Simple components easy to reuse everywhere.

- molecules:

More consistent components build to be reused in many cases.

- organisms:

Bigger components no easy diserved to be reused in some cases.

- templates:

Components required to modify global aspect of organisms.
