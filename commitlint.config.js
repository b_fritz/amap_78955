module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-case': [2, 'always', 'upper-case'],
    'scope-enum': [
      2,
      'always',
      ['FRONT', 'BACK', 'FULL', 'ARCHI', 'STYLE', 'CONFIG', 'DOC'],
    ],
    'subject-full-stop': [2, 'always', '.'],
    'subject-case': [2, 'always', 'sentence-case'],
  },
};
