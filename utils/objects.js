export function isNotEmpty(value) {
  return value && value !== null;
}
