import { resolve } from 'path';

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'amap_78955',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    { path: '~/components/atoms', extensions: ['vue', 'ts'] },
    { path: '~/components/molecules', extensions: ['vue', 'ts'] },
    { path: '~/components/organisms', extensions: ['vue', 'ts'] },
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/style-resources', // Global SCSS styles, variables and mixins without import in each file.
    [
      'nuxt-gmaps',
      {
        key: process.env.GOOGLE_MAP_KEY,
      },
    ],
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  alias: {
    utils: resolve(__dirname, './utils'),
  },
  // Global SCSS styles, variables and mixins without import in each file.
  styleResources: {
    scss: ['~/assets/scss/main.scss'],
  },
  // Environment variables: https://nuxtjs.org/docs/directory-structure/nuxt-config#env
  publicRuntimeConfig: {
    googleMapKey: process.env.GOOGLE_MAP_KEY,
  },
  serverMiddleware: {
    '/api': '~/api',
  },
};
