module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'prettier',
    'eslint:recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: ['node', 'prettier', 'prefer-arrow'],
  // add your custom rules here
  rules: {
    'prettier/prettier': ['error'],
    'node/exports-style': ['error', 'module.exports'],
    'node/no-unpublished-require': 0,
    'no-underscore-dangle': 0,
    'no-console': [
      'error',
      {
        allow: ['info', 'debug', 'error', 'assert'],
      },
    ],
    'arrow-body-style': ['error', 'as-needed'],
  },
};
