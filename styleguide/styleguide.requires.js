import Vue from 'vue';
import * as BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);
