const { join } = require('path');
const { getWebpackConfig } = require('nuxt');
const { version, name } = require('../package.json');

const FILTERED_PLUGINS = [
  'WebpackBarPlugin',
  'VueSSRClientPlugin',
  'HotModuleReplacementPlugin',
  'FriendlyErrorsWebpackPlugin',
  'HtmlWebpackPlugin',
];

const docSiteUrl =
  process.env.DEPLOY_PRIME_URL || 'https://vue-styleguidist.github.io';

/** @type import("vue-styleguidist").Config */
module.exports = async () => {
  // get the webpack config directly from nuxt
  const nuxtWebpackConfig = await getWebpackConfig();

  const webpackConfig = {
    module: {
      rules: [
        ...nuxtWebpackConfig.module.rules.filter(
          // remove the eslint-loader
          a => a.loader !== 'eslint-loader',
        ),
      ],
    },
    resolve: { ...nuxtWebpackConfig.resolve },
    plugins: [
      ...nuxtWebpackConfig.plugins.filter(
        // And some other plugins that could conflict with ours
        p => !FILTERED_PLUGINS.includes(p.constructor.name),
      ),
    ],
  };

  return {
    title: name,
    version,
    components: ['../components/**/*.vue'],
    require: [
      join(__dirname, 'styleguide.requires.js'),
      join(__dirname, '../assets/scss/main.scss'),
      join(__dirname, '../node_modules/bootstrap/dist/css/bootstrap.min.css'),
      join(
        __dirname,
        '../node_modules/bootstrap-vue/dist/bootstrap-vue.min.css',
      ),
    ],
    ribbon: {
      text: 'Back to examples',
      url: `${docSiteUrl}/Examples.html`,
    },
    webpackConfig,
    usageMode: 'expand',
    styleguideDir: 'dist',
    pagePerSection: true,
    codeSplit: true,
    compilerConfig: {
      objectAssign: 'Object.assign',
      transforms: {
        // Don't throw on ESM imports, we transpile them ourselves
        modules: false,
        // Enable tagged template literals for styled-components
        dangerousTaggedTemplateString: true,
        // to make async/await work by default (no transformation)
        asyncAwait: false,
      },
    },
    sections: [
      {
        name: 'Introduction',
        content: '../README.md',
      },
      {
        name: 'Todolist',
        content: '../TODOLIST.md',
      },
      {
        name: 'Utils',
        content: '../utils/README.md',
      },
      {
        name: 'Components',
        content: '../components/README.md',
        sectionDepth: 2,
        sections: [
          {
            name: 'Atoms',
            components: '../components/atoms/**/*.vue',
          },
          {
            name: 'Molecules',
            components: '../components/molecules/**/*.vue',
          },
          {
            name: 'Organisms',
            components: '../components/organisms/**/*.vue',
          },
        ],
      },
      {
        name: 'API',
        content: '../api/README.md',
      },
    ],
  };
};
