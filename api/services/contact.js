export function sendContactMail({ email, subject, text }) {
  // should send an email to the back-end
  return Promise.resolve({
    statusCode: 200,
    body: {
      message: `Email ${subject} from ${email} was sended. Content: ${text}`,
    },
  });
}
