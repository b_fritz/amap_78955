export function testOK() {
  return Promise.resolve({
    statusCode: 200,
    body: {
      ok: true,
    },
  });
}
