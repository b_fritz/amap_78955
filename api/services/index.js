import { testOK } from './mock';
import { sendContactMail } from './contact';

export { testOK, sendContactMail };
