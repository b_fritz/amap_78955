import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';

const app = express();
app.use(bodyParser.json());
app.use('/', routes());

if (require.main === module) {
  const port = process.env.PORT || 3001;
  app.listen(port, () => {
    console.info(`API server listening on port ${port}`);
  });
}

module.exports = app;
