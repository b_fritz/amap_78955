import fs from 'fs';
import { Router } from 'express';
import multer from 'multer';
import { validate } from 'express-validation';

/**
 * @description Handles routes' functions and returns result and errors.
 * @param {*} routeFn
 * @return Api response or an error.
 */
const route = routeFn => async (req, res) => {
  try {
    const { statusCode = 200, body } = await routeFn({
      params: req.params,
      query: req.query,
      body: req.body,
      file: req.file,
    });
    res.status(statusCode).json(body);
  } catch (error) {
    res
      .status(error?.response?.status || 500)
      .json(error?.response?.data || error?.message || 'No error message!');
  }
};

// it allows route params.
const fixRoutePath = path => path.replace(/\[/g, ':').replace(/\]/g, '');

// Get ./routes files and filters tests/snapshots files.
const findRoutes = (dir = __dirname) => {
  let routes = [];
  const list = fs.readdirSync(dir);
  list.forEach(file => {
    const filepath = `${dir}/${file}`;
    const stat = fs.statSync(filepath);
    if (stat?.isDirectory()) {
      if (!['__snapshots__'].includes(file))
        routes = [...routes, ...findRoutes(filepath)];
    } else if (!file.endsWith('.spec.js')) {
      routes.push(filepath.replace(__dirname, '').replace('.js', ''));
    }
  });
  return routes;
};

export default () => {
  const router = new Router();
  findRoutes().forEach(r => {
    const currentRoute = require(`.${r}`);
    if (currentRoute?.default) {
      const dRoute = currentRoute.default;
      const methods = Object.keys(dRoute);
      methods.forEach(method => {
        if (dRoute[method].isSingleFile) {
          return router[method](
            fixRoutePath(r),
            multer().single('file'),
            route(dRoute[method].function),
          );
        }
        if (dRoute[method].validation) {
          return router[method](
            fixRoutePath(r),
            validate(
              dRoute[method].validation,
              { keyByField: true },
              { abortEarly: false },
            ),
            route(dRoute[method].function),
          );
        }
        return router[method](fixRoutePath(r), route(dRoute[method].function));
      });
    }
  });
  return router;
};
