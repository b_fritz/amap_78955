import Joi from 'joi';
import { sendContactMail } from '../services';

export default {
  post: {
    function: async ({ body }) => {
      const { statusCode = 500, body: message = 'No content' } =
        await sendContactMail(body);
      return { statusCode, body: message };
    },
    validation: {
      body: Joi.object().keys({
        email: Joi.string().required(),
        subject: Joi.string().required(),
        text: Joi.string().required(),
      }),
    },
  },
};
