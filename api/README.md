## Structure

```markdown
api
│ README.md
│ index.js  
│
└─── routes
│ │ index.js
│ │ aFile.js
│ │
│ └─── [param]
│ | │ root.js
│ | │ otherFile.js
│ | │  
│ | └─── otherFolder
│ | │ root.js
│ | │ otherFile2.js
│ └─── otherFolder2
│ │ root.js
│  
└─── services
| | index.js
│ └─── serviceFolder
│ │ myService.js
```

## Folders

**These explanations follow the structure of the previous section.**

## Tests unitaire

You have to use `**/*.test.js` extension.

## Main directory

Contains the `README.md` for usages and good pratices and the `index.js` to launch the server.

## Middlewares

All middlewares are optional.

### Express-validation and Joi

To use Joi, you have to add an attribute **validation in the desired method of your route**.

Express-validation configuration is: `{ keyByField: true }, { abortEarly: false }`.

Sample:

```javascript
// in api/routes/toto/root.js
import Joi from 'joi';

export default {
  post: {
    function: async ({ body }) => {
      // Something to do.
    },
    validation: {
      query: Joi.object({
        // your required/optional fields
      }),
      body: Joi.object({
        // your required/optional fields
      }),
    },
  },
};
```

For more informations about Joi please [follow this link](https://joi.dev/api/?v=17.5.0 'joi documentation').

For more informations about Express-validation please [follow this link](https://www.npmjs.com/package/express-validation 'express-validation documentation').

### Multer

Multer is a package to upload files.

Currently implemented to upload a single file.

_@todo: find a way to upload single and multiple files and specify the destination folder(s)._

For more more informations about multer please [follow this link](https://www.npmjs.com/package/multer 'multer documentation').

## routes

### The Structure

- `routes/index.js`: File in charge to build routes' path according with `routes/` tree structure, filters `**.**.test.js`, `__snapshots__` folders and attach middlewares;
- params: To add a param, you have use this syntaxe to name your folder or file `[myParamName]`;
- root path: a file named root.js will take the path of the parent folder `api/routes/toto/root.js` == url == > 'my-site://toto';

### Files pattern

Each file exports an oject. This object contains the methods (get, post, put, delete). Every method is optional.

Each method contains an attribute function. This function should be async.

This files should not contains business logic. They have to call services and return there responses. This is just handlers.

Sample:

```javascript
import { testOK } from './services';

export default {
  get: {
    function: async reqParams => {
      const { statusCode = 500, body } = await testOK();
      return { statusCode, body };
    },
  },
};
```

## services

### Usages

Each file exports his fonctions, they are imported and exposed in the file index.js.

This files contains the business logic and make the extern api calls.

Errors should be managed here. The routes will handle them.

File index:

```javascript
import { testOK } from './services/mock.js';

export { testOK };
```

Sample:

```javascript
export function testOK() {
  // BE call. Must return a promise resolved or a promise rejected managed.
  return Promise.resolve({
    statusCode: 200,
    body: {
      // your response
    },
  });
}
```
