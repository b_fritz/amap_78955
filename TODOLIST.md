## Rename project to amap_78955

## API

Clean's up routes with samples:

- [x] File test/root.js;
- [x] Add flag for wrong api methods;
- [x] Clean's up services to test routes;
- [x] Removes useless xml2js package;
- [x] Add a README.md to describe the structure, right and wrong usages, middlewares implemented.

## FRONT

Structure the code:

- [x] Add folders /components/{atoms|molecules|organisms} & /utils & /api/services;
- [x] Add alias to the config file and jest.config.js;
- [x] Remove useless test folder.

## DOCUMENTATION

- [x] Clean up api/README.md
- [x] Styleguidist? Or something else?
- [x] Should read README.md files?
- [x] Disable markdownlint MD041 globaly;
- [x] Push it online.

## Heroku

## CI

- [ ] Run all tests on gitlab;
- [ ] Build on gitlab push;
- [ ] deploy to Heroku on gitlab.

## pages

### index

- [ ] Find a way to reduce the p tags (with content?);

### Contacts (front)

- [x] Structure the page;
- [x] Add component molecule for googleMaps;
- [x] Add Form to contact us (only front);
- [x] Use nuxt process.env for api key google.

### Contacts (back)

- [x] Add route for contact us submit;

## DOC

Components doesn't render in styleguidist.

- [x] fix the doc. [may be see](https://github.com/vue-styleguidist/vue-styleguidist/tree/dev/examples/router)
