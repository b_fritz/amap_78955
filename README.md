## Commitlint convention

A commit message must follow this format: **`type(SCOPE): Subject.`**.

- Type: one of **`['feat', 'fix', 'docs', 'style', 'refactor', 'test', 'revert']`**;
- Scope: one of **`['FRONT', 'BACK', 'FULL', 'ARCHI', 'STYLE', 'CONFIG']`**;
- Subject: a sentence that **start with a capital letter** and **ends with a period**.

## REAME.md files

First line is **_markdownlint-disable MD041_** commented because te first line is an h1 included in the styleguide.config.js. So we start te file with an h2 tag.

For more information [see](https://github.com/DavidAnson/markdownlint/blob/main/doc/Rules.md#md041 'markdown linter MD041')

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).

### `api`

Contains the gateway of our back-end. Contains routes and services. See the README.md.

### `.env`

Contains environment variables. @see .env.sample for list of this variables. They are holded by nuxt runtime config.

More informtion about runtime config in [the documentation](https://nuxtjs.org/docs/directory-structure/nuxt-config#env)

## Heroku

### To deploy

- Install the CLI;

- Add remote:

```bash
heroku login
heroku git:remote -a amap-78955
```

- Push your branch to Heroku:

```bash
git push heroku yourBranch:main
```

See your work at <https://amap-78955.herokuapp.com/>
